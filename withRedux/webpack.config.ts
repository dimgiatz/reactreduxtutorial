import * as path from "path";
import * as Webpack from "webpack";
import * as Constants from "./src/constants/constants";
import HtmlWebpackPlugin from "html-webpack-plugin";
import WebpackDevServer from "webpack-dev-server";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
// tslint:disable-next-line:no-var-requires
const NodeEnvPlugin = require("node-env-webpack-plugin");

type Mode = "development" | "production" | "none";

const mode = process.env["NODE_ENV"] as Mode;

const production = mode === "production";

const entry: () => string[] = () => {
    const entries = ["./src/index.tsx"];

    if (production) {
        return entries;
    } else {
        return [
            ...entries,
            // Bundle the client for webpack-dev-server and connect to the provided endpoint.
            `webpack-dev-server/client?http://localhost:${Constants.appPort}`,
            // Bundle the client for hot reloading. only- means to only hot reload for successful updates.
            "webpack/hot/only-dev-server",
        ];
    }
};

const output: Webpack.Output = {
    filename: production
        ? `${Constants.appName}.[hash].js`
        : `${Constants.appName}.js`,
    path: path.resolve(__dirname + "/dist"),
    publicPath: "/",
};

// Enable sourcemaps for debugging webpack`s output.
const devtool: Webpack.Options.Devtool = production
    ? "nosources-source-map"
    : "source-map";

const devServer: WebpackDevServer.Configuration = {
    inline: true,
    port: Constants.appPort,
    historyApiFallback: true,
    hot: !production,
    contentBase: path.resolve(__dirname, "src"),
    publicPath: "/",
    // match the output `publicPath`
};

const resolve: Webpack.Resolve = {
    extensions: [".ts", ".tsx", ".js", ".json"],
};

const webpackModule: Webpack.Module = {
    rules: [
        // All files with a ".ts" or ".tsx" extension will be handled by "ts-loader".
        {
            test: /\.tsx?$/,
            loader: "ts-loader",
            include: /src/,
        },

        // All files with a ".css" extension will be handled by "css-loader".
        {
            test: /\.css$/,
            use: [
                MiniCssExtractPlugin.loader,
                "css-loader",
            ],
            include: /src/,
        },

        // All output ".js" files will have any sourcemaps re-processed by "source-map-loader".
        {
            enforce: "pre",
            test: /\.js$/,
            loader: "source-map-loader",
            exclude: /node_modules/,
        },

        // All images with a ".jpe?g", ".png", ".gif" or ".svg" extension will be handled by "file-loader".
        {
            test: /\.(jpe?g|png|gif|svg)$/i,
            loader: "file-loader",
            options: {
                name: "[path][name].[ext]",
            },
        },
    ],
};

const plugins: Webpack.Plugin[] = [
    new MiniCssExtractPlugin({
        filename: production
            ? `${Constants.appName}.[hash].css`
            : `${Constants.appName}.css`,
    }),
    new HtmlWebpackPlugin({
        template: production
            ? "./src/index.production.html"
            : "./src/index.html",
    }),
    new Webpack.HotModuleReplacementPlugin(),
    new NodeEnvPlugin(),
];

// When importing a module whose path matches one of the following, just
// assume a corresponding global variable exists and use that instead.
// This is important because it allows us to avoid bundling all of our
// dependencies, which allows browsers to cache those libraries between builds.
const externals: Webpack.ExternalsElement[] = [
    {
        // FIXME: Add "connected-react-router" once cdn is found.
        "react": "React",
        "React": "React",
        "react-dom": "ReactDOM",
        "redux": "Redux",
        "react-redux": "ReactRedux",
        "react-router": "ReactRouter",
        "redux-logger": "reduxLogger",
        "redux-saga": "ReduxSaga",
        "redux-saga/effects": "ReduxSaga.effects",
        "oidc-client": "Oidc",
        "@material-ui/core": "MaterialUI"
    },
];

const config: Webpack.Configuration = {
    devServer,
    devtool,
    entry,
    externals,
    module: webpackModule,
    output,
    resolve,
    plugins,
    mode,
};

export default config;
