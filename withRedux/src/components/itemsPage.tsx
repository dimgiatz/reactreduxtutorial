import * as React from "react";
import { Grid, makeStyles } from "@material-ui/core";
import { ItemsListContainer } from '../containers/itemsListContainer';
import { SelectedItem } from "./selectedItem";

const useStyles = makeStyles((theme) => ({
    root: {
        position: "relative",
        width: "100vw",
        height: "100vh",
        overflow: "hidden",
        backgroundColor: theme.palette.background.default,
    },
    container: {
        padding: 50,
    },
}));

interface ItemsPageDataProps { }

interface ItemsPageDispatchProps { }

type ItemsPageProps = ItemsPageDataProps & ItemsPageDispatchProps;

const ItemsPage: React.FunctionComponent<ItemsPageProps> = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container className={classes.container} justify="center" spacing={3}>
                <Grid item xs={12} md={4}>
                    <ItemsListContainer />
                </Grid>
                <Grid item xs={12} md={4}>
                    <SelectedItem />
                </Grid>
            </Grid>
        </div>
    );
};

export {
    ItemsPage,
    ItemsPageProps,
    ItemsPageDataProps,
    ItemsPageDispatchProps,
};
