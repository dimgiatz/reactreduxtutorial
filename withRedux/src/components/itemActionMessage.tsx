import * as React from "react";
import { Typography } from '@workloud/wl-tools-components';
import { ItemAction } from "../model/itemAction";

interface ItemActionMessageDataProps {
    action: ItemAction;
}

interface ItemActionMessageDispatchProps { }

type ItemActionMessageProps = ItemActionMessageDataProps & ItemActionMessageDispatchProps;

const ItemActionMessage: React.FunctionComponent<ItemActionMessageProps> = (props: ItemActionMessageProps) => {
    const { action } = props;

    if (!action) {
        return null;
    }

    const getActionMessage = () => {
        switch (action.type) {
            case "add":
                return `${action.newItem} was added.`
            case "update":
                return `${action.oldItem} was updated to ${action.newItem}.`
            case "delete":
                return `${action.oldItem} was deleted.`
            default:
                return "";
        }
    }

    return <Typography content={getActionMessage()} variant="paragraph1" color="secondary" />;
}

export {
    ItemActionMessageProps,
    ItemActionMessageDataProps,
    ItemActionMessageDispatchProps,
    ItemActionMessage,
};
