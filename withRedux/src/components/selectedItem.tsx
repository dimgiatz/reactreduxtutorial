import * as React from "react";
import { Grid, makeStyles, Paper } from "@material-ui/core";
import { ItemActionMessageContainer } from '../containers/itemActionMessageContainer';
import { ItemEditorContainer } from "../containers/itemEditorContainer";

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: 8,
    },
}));

interface SelectedItemDataProps {}

interface SelectedItemDispatchProps {}

type SelectedItemProps = SelectedItemDataProps & SelectedItemDispatchProps;

const SelectedItem: React.FunctionComponent<SelectedItemProps> = () => {
    const classes = useStyles();

    return (
        <Paper className={classes.paper}>
            <Grid container direction="column" spacing={1}>
                <Grid item>
                    <ItemEditorContainer />
                </Grid>
                <Grid item>
                    <ItemActionMessageContainer />
                </Grid>
            </Grid>
        </Paper>
    );
};

export {
    SelectedItem,
};
