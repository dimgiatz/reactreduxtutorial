import { ListItem } from '@workloud/wl-tools-components';

export const brandName: string = "indeavor";
export const appName: string = "redux-tutorial";
export const appPort: number = 9020;
export const idListSelector: string = "list-selector";

export const items: ListItem[] = [
    { id: "0", title: "Item 0" },
    { id: "1", title: "Item 1" },
    { id: "2", title: "Item 2" },
    { id: "3", title: "Item 3" },
    { id: "4", title: "Item 4" },
];
