import "./css/app.css";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { getStore } from './configureStore';
import { ItemsPage } from "./components/itemsPage";

const render = () => {
    ReactDOM.render(
        <Provider store={getStore()}>
            <ItemsPage />
        </Provider>,
        document.getElementById("app"),
    );

    if (module.hot) {
        module.hot.accept();
    }
}

render();