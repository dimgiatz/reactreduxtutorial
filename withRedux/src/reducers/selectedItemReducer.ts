import * as Actions from "../actions";
import { initializeAppState } from '../appState';

const selectedItemReducer = (state: string = initializeAppState().selectedItem, action: Actions.SelectedItemAction | Actions.ItemsAction): string => {
    switch (action.type) {
        case Actions.SelectedItemActionTypes.SelectItem:
            return action.id;
        case Actions.SelectedItemActionTypes.UnselectItem:
            return "";
        case Actions.ItemsActionTypes.DeleteItem:
            return "";
        default:
            return state;
    }
};

export {
    selectedItemReducer,
}