import { combineReducers } from 'redux';
import { AppState } from '../appState';
import { itemActionReducer } from './itemActionReducer';
import { itemsReducer } from './itemsReducer';
import { selectedItemReducer } from './selectedItemReducer';

const rootReducer = () => combineReducers<AppState>({
    items: itemsReducer,
    selectedItem: selectedItemReducer,
    itemAction: itemActionReducer
});

export default rootReducer;