import { ListItem } from '@workloud/wl-tools-components';

import * as Actions from "../actions";
import { initializeAppState } from '../appState';

const itemsReducer = (state: ListItem[] = initializeAppState().items, action: Actions.ItemsAction): ListItem[] => {
    switch (action.type) {
        case Actions.ItemsActionTypes.AddItem:
            return [...state, { id: (Math.max(...state.map((item) => Number(item.id))) + 1).toString(), title: action.title }];
        case Actions.ItemsActionTypes.UpdateItem:
            return [...state.map((item) => item.id === action.item.id ? { id: action.item.id, title: action.newTitle } : item)];
        case Actions.ItemsActionTypes.DeleteItem:
            return [...state.filter((item) => item.id !== action.item.id)];
        default:
            return state;
    }
};

export {
    itemsReducer
}