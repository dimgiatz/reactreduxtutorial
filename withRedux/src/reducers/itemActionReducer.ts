import * as Actions from "../actions";
import { initializeAppState } from '../appState';
import { ItemAction } from '../model/itemAction';

const itemActionReducer = (state: ItemAction = initializeAppState().itemAction, action: Actions.ItemsAction | Actions.SelectedItemAction): ItemAction => {
    switch (action.type) {
        case Actions.ItemsActionTypes.AddItem:
            return { type: "add", oldItem: null, newItem: action.title };
        case Actions.ItemsActionTypes.UpdateItem:
            return { type: "update", oldItem: action.item.title, newItem: action.newTitle };
        case Actions.ItemsActionTypes.DeleteItem:
            return { type: "delete", oldItem: action.item.title };
        case Actions.SelectedItemActionTypes.UnselectItem:
            return null;
        case Actions.SelectedItemActionTypes.SelectItem:
            return null;
        default:
            return state;
    }
};

export {
    itemActionReducer
}