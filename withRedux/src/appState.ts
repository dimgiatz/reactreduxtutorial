import * as Constants from "./constants"
import { ListItem } from "@workloud/wl-tools-components";
import { ItemAction } from "./model/itemAction";

interface AppState {
    items: ListItem[];
    itemAction: ItemAction;
    selectedItem: string;
}

const initializeAppState = (): AppState => ({
    items: Constants.items,
    itemAction: null,
    selectedItem: null,
});

export {
    AppState,
    initializeAppState,
}