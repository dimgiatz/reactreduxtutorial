import rootReducer from "./reducers/index";
import { createStore } from "redux";
import { initializeAppState } from "./appState";

function configureStore() {
    const storeResult = createStore(rootReducer(), initializeAppState());

    return storeResult;
}

const getStore = () => configureStore();

if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept("./reducers/index", () => {
        const nextRootReducer = require("./reducers/index");
        getStore().replaceReducer(nextRootReducer);
    });
}

export {
    configureStore,
    getStore,
};
