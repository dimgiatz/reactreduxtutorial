import * as Actions from "../actions";
import { connect } from "react-redux";
import { AppState } from "../appState";
import { Dispatch } from "redux";
import { ListItem } from '@workloud/wl-tools-components';
import { ItemEditor, ItemEditorDataProps, ItemEditorDispatchProps } from "../components/itemEditor";

const mapStateToProps = (state: AppState): ItemEditorDataProps => {
    return {
        item: state.items.find((item) => item.id === state.selectedItem),
    };
};

const mapDispatchToProps = (dispatch: Dispatch): ItemEditorDispatchProps => {
    return {
        add: (title: string) => {
            dispatch(Actions.addItem(title));
        },
        update: (item: ListItem, newTitle: string) => {
            dispatch(Actions.updateItem(item, newTitle));
        },
        cancel: () => {
            dispatch(Actions.unselectItem());
        }
    };
};

const ItemEditorContainer = connect(mapStateToProps, mapDispatchToProps)(ItemEditor);

export {
    ItemEditorContainer,
};
