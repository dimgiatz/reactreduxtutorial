import * as Actions from "../actions";
import { connect } from "react-redux";
import { AppState } from "../appState";
import { Dispatch } from "redux";
import { ItemsList, ItemsListDataProps, ItemsListDispatchProps } from '../components/itemsList';
import { ListItem } from '@workloud/wl-tools-components';

const mapStateToProps = (state: AppState): ItemsListDataProps => {
    return {
        items: state.items,
        selectedItem: state.selectedItem,
    };
};

const mapDispatchToProps = (dispatch: Dispatch): ItemsListDispatchProps => {
    return {
        deleteItem: (item: ListItem) =>{
            dispatch(Actions.deleteItem(item));
        },
        selectItem: (id: string) => {
            dispatch(Actions.selectItem(id));
        } 
    };
};

const ItemsListContainer = connect(mapStateToProps, mapDispatchToProps)(ItemsList);

export {
    ItemsListContainer,
};
