import { connect } from "react-redux";
import { AppState } from "../appState";
import { Dispatch } from "redux";
import { ItemActionMessage, ItemActionMessageDataProps, ItemActionMessageDispatchProps } from "../components/itemActionMessage";

const mapStateToProps = (state: AppState): ItemActionMessageDataProps => {
    return {
        action: state.itemAction,
    };
};

const mapDispatchToProps = (dispatch: Dispatch): ItemActionMessageDispatchProps => {
    return {};
};

const ItemActionMessageContainer = connect(mapStateToProps, mapDispatchToProps)(ItemActionMessage);

export {
    ItemActionMessageContainer,
};
