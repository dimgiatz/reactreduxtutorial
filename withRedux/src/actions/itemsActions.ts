import { ListItem } from '@workloud/wl-tools-components';

enum ItemsActionTypes {
    AddItem = "ADD_ITEM",
    UpdateItem = "UPDATE_ITEM",
    DeleteItem = "DELETE_ITEM",
}

type Add = {
    type: ItemsActionTypes.AddItem,
    title: string,
};

type Update = {
    type: ItemsActionTypes.UpdateItem,
    item: ListItem,
    newTitle: string,
};

type Delete = {
    type: ItemsActionTypes.DeleteItem,
    item: ListItem,
};

type ItemsAction =
    Add |
    Update |
    Delete;

const addItem = (title: string): Add => ({
    type: ItemsActionTypes.AddItem,
    title,
});

const updateItem = (item: ListItem, newTitle: string): Update => ({
    type: ItemsActionTypes.UpdateItem,
    item,
    newTitle,
});

const deleteItem = (item: ListItem): Delete => ({
    type: ItemsActionTypes.DeleteItem,
    item,
});

export {
    ItemsActionTypes,
    ItemsAction,
    addItem,
    updateItem,
    deleteItem
};
