enum SelectedItemActionTypes {
    SelectItem = "SELECT_ITEM",
    UnselectItem = "UNSELECT_ITEM",
}

type Select = {
    type: SelectedItemActionTypes.SelectItem,
    id: string
};

type Unselect = {
    type: SelectedItemActionTypes.UnselectItem,
};

type SelectedItemAction =
    Select |
    Unselect;

const selectItem = (id: string): Select => ({
    type: SelectedItemActionTypes.SelectItem,
    id
});

const unselectItem = (): Unselect => ({
    type: SelectedItemActionTypes.UnselectItem,
});

export {
    SelectedItemActionTypes,
    SelectedItemAction,
    selectItem,
    unselectItem,
};
