interface ItemAction {
    type: "add" | "update" | "delete";
    oldItem?: string;
    newItem?: string;
}

export {
    ItemAction,
}
