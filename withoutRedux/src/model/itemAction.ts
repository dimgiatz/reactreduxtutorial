import { ListItem } from "@workloud/wl-tools-components";

interface ItemAction {
    type: "add" | "update" | "delete";
    oldItem?: ListItem;
    newItem?: ListItem;
}

export {
    ItemAction,
}
