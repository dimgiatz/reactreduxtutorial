import * as React from "react";
import { Button, Input } from "@material-ui/core";
import { ListItem } from '@workloud/wl-tools-components';

interface ItemEditorDataProps {
    item: ListItem;
}

interface ItemEditorDispatchProps {
    update: (title: string) => void;
    add: (title: string) => void;
    cancel: () => void;
}

type ItemEditorProps = ItemEditorDataProps & ItemEditorDispatchProps;

const ItemEditor: React.FunctionComponent<ItemEditorProps> = (props: ItemEditorProps) => {
    const { item, update, add, cancel } = props;
    const [title, setTitle] = React.useState(item?.title || "");

    React.useEffect(() => setTitle(item?.title || ""), [item]);

    return (
        <>
            <Input
                value={title}
                onChange={(event) => { setTitle(event.target.value.toString()) }}
            />
            {
                item ?
                    <>
                        <Button onClick={() => update(title)}>Update</Button>
                        <Button onClick={() => cancel()}>Cancel</Button>
                    </>
                    :
                    <Button onClick={() => add(title)}>Add</Button>
            }
        </>
    );
};

export {
    ItemEditor,
};
