import * as React from "react";
import { Grid, makeStyles, Paper } from "@material-ui/core";
import { ListItem } from '@workloud/wl-tools-components';
import { ItemEditor } from "./itemEditor";
import { ItemAction } from "../model/itemAction";
import { ItemActionMessage } from "./itemActionMessage";

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: 16
    },
}));

interface ItemDataProps {
    item: ListItem;
    action: ItemAction;
}

interface ItemDispatchProps {
    update: (title: string) => void;
    add: (title: string) => void;
    cancel: () => void;
}

type ItemProps = ItemDataProps & ItemDispatchProps;

const Item: React.FunctionComponent<ItemProps> = (props: ItemProps) => {
    const classes = useStyles();
    const { item, action, update, add, cancel } = props;

    return (
        <Paper className={classes.paper}>
            <Grid container direction="column" spacing={1}>
                <Grid item>
                    <ItemEditor item={item} add={add} update={update} cancel={cancel} />
                </Grid>
                <Grid item>
                    <ItemActionMessage action={action} />
                </Grid>
            </Grid>
        </Paper>
    );
};

export {
    Item,
};
