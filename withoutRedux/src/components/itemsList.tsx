import * as React from "react";
import * as Constants from "../constants";
import { IconButton } from "@material-ui/core";
import DeleteIcon from '@material-ui/icons/Delete';
import { List, ListItem } from '@workloud/wl-tools-components';

interface ItemsListDataProps {
    items: ListItem[];
    selectedItem: string;
}

interface ItemsListDispatchProps {
    deleteItem: (id: string) => void;
    selectItem: (id: string) => void;
}

type ItemsListProps = ItemsListDataProps & ItemsListDispatchProps;

const ItemsList: React.FunctionComponent<ItemsListProps> = (props: ItemsListProps) => {
    const { items, selectedItem, selectItem, deleteItem } = props;

    const getDeleteIcon = (id: string) =>
        <IconButton onClick={(event) => {
            event.stopPropagation();
            deleteItem(id);
        }}>
            <DeleteIcon />
        </IconButton>;

    const getListItems = (): ListItem[] => items.map((item) => ({
        ...item,
        isSelected: item.id === selectedItem,
        onClick: () => selectItem(item.id),
        rightAction: getDeleteIcon(item.id),
    }));

    return <List
        id={Constants.idListSelector}
        items={getListItems()}
        height="78vh"
    />
};

export {
    ItemsList,
};
