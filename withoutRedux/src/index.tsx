import "./css/app.css";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { ItemsPage } from './components/itemsPage';

const render = () => {
    ReactDOM.render(
        <ItemsPage></ItemsPage>,
        document.getElementById("app"),
    );
}

render();