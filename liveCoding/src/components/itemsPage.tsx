import * as React from "react";
import * as Constants from "../constants";
import { Grid, makeStyles, Paper } from "@material-ui/core";
import { ListItem } from '@workloud/wl-tools-components';
import { Item } from './item';
import { ItemsList } from "./itemsList";
import { ItemAction } from "../model/itemAction";

const useStyles = makeStyles((theme) => ({
    root: {
        padding: "50px",
        width: "100vw",
        height: "100vh",
        backgroundColor: theme.palette.background.default,
    },
}));

interface ItemsPageDataProps { }

interface ItemsPageDispatchProps { }

type ItemsPageProps = ItemsPageDataProps & ItemsPageDispatchProps;

const ItemsPage: React.FunctionComponent<ItemsPageProps> = (props: ItemsPageProps) => {
    const classes = useStyles();

    const [items, setItems] = React.useState<ListItem[]>(Constants.items);
    const [selectedItem, setSelectedItem] = React.useState("");
    const [action, setAction] = React.useState<ItemAction>(null);

    const addItem = (title: string) => {
        const newItem: ListItem = { id: items.length.toString(), title };

        setItems([...items, newItem]);
        setAction({ type: "add", newItem });
    };
    const deleteItem = (id: string) => {
        const deletedItem: ListItem = items.find((item) => item.id === id);

        setItems(items.filter((item) => item.id !== id));
        setAction({ type: "delete", oldItem: deletedItem });
    };
    const updateItem = (title: string) => {
        const oldItem = items.find((item) => item.id === selectedItem);
        const newItem = { id: selectedItem, title };

        setItems(items.map((item) => item.id === selectedItem ? newItem : item));
        setAction({ type: "update", oldItem, newItem });
    };
    const selectItem = (id: string) => {
        setAction(null);
        setSelectedItem(id)
    };

    const cancel = () => {
        setAction(null);
        setSelectedItem("");
    };

    return (
        <Grid className={classes.root} container justify="center" spacing={3}>
            <Grid item xs={12} md={4}>
                <ItemsList
                    items={items}
                    selectedItem={selectedItem}
                    selectItem={selectItem}
                    deleteItem={deleteItem}
                />
            </Grid>
            <Grid item xs={12} md={4}>
                <Paper>
                    <Item
                        item={items.find((item) => item.id === selectedItem)}
                        action={action}
                        add={addItem}
                        update={updateItem}
                        cancel={cancel}
                    />
                </Paper>
            </Grid>
        </Grid>
    );
};

export {
    ItemsPage,
};
